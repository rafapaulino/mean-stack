﻿var http = require("http");

var server = http.createServer();
server.on('request',function(request, response) {
	response.writeHead(200);
	response.write("servidor mandou os primeiros dados do request");
	setTimeout(function(){
		response.write("Segundo pacote");
		response.end();
	}, 5000);
});
server.listen(8882);
server.on('close',function(){
	console.log("Servidor fechado");
});
console.log("Servidor iniciado");

