var express = require('express');
var router = express.Router();

var UserModel = require('../models/user');	

router.get('/list', function(req, res, next) {
  	UserModel.list(req, res);
});

router.get('/:id', function(req, res, next) {
  	UserModel.get(req, res);
});

router.post('/create', function(req, res, next) {
  	UserModel.create(req, res);
});

router.put('/update/:id', function(req, res, next) {
  	UserModel.update(req, res);
});

router.delete('/delete/:id', function(req, res, next) {
  	UserModel.delete(req, res);
});

module.exports = router;