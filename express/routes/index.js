var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/:index', function(req, res) {
  res.render('index', { title: 'School of Net' });
});

router.get('/partials/:name', function(req, res) {
	console.log(req);
  var name = req.params.name;
  res.render('partials/' + name);
});

router.get('/angular/:diretorio/:name', function(req, res) {
  var diretorio = req.params.diretorio;
  var name = req.params.name;
  res.render(diretorio + '/' + name);
});

module.exports = router;