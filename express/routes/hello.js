var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/:username?', function(req, res, next) {
  
  console.log(req.params);
  var name = req.params.username;
  console.log('name = '+name);
  var data = {
  		nome: 'Rafael Paulino',
  		site: 'www.rafapaulino.com',
  		username: name
  };

  res.json(data);
});

module.exports = router;
