angular.module('sonApp.services', ['ngResource'])
	.factory('UsersSrv', ['$resource', 
		function ($resource) {
		
			return $resource('/user/:id', 
				{
					id: '@id'
				}, 
				{
					update: {
						method: "PUT",
						url: "/user/update/:id"
					}
				}
			)

		}
	]);

/* 
http://www.sitepoint.com/creating-crud-app-minutes-angulars-resource/ 
https://docs.angularjs.org/api/ngResource/service/$resource
*/